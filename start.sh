#!/bin/bash

set -x

random_string() {
	LC_CTYPE=C tr -dc 'a-zA-Z0-9' < /dev/urandom | head -c32
}

cat <<-EOF > "/app/data/README.md"
# README
Make sure to check out https://github.com/42wim/matterbridge/wiki/How-to-create-your-config on how to configure your own bridge.

After updating "matterbridge.toml" you can restart Matterbridge by running "supervisorctl restart matterbridge".
EOF

if [ ! -e /app/data/matterbridge.toml ]; then
        cat <<-EOF > "/app/data/matterbridge.toml"
# used for healthcheck
[api.myapi]
BindAddress="127.0.0.1:4242"
Buffer=1000
RemoteNickFormat="{NICK}"
Token="$(random_string)"

[rocketchat.myrocketchat]
#The rocketchat hostname. (prefix it with http or https)
#REQUIRED (when not using webhooks)
Server="https://rocketchat.domain.com"

#login/pass of your bot. (login needs to be the login with email address! user@domain.com)
#Use a dedicated user for this and not your own!
#REQUIRED (when not using webhooks)
Login="yourlogin@domain.com"
Password="yourpass"

#Whether to prefix messages from other bridges to rocketchat with the sender's nick.
#Useful if username overrides for incoming webhooks isn't enabled on the
#rocketchat server. If you set PrefixMessagesWithNick to true, each message
#from bridge to rocketchat will by default be prefixed by the RemoteNickFormat setting. i
#if you're using login/pass you can better enable because of this bug:
#https://github.com/RocketChat/Rocket.Chat/issues/7549
#OPTIONAL (default false)
PrefixMessagesWithNick=true

#Your bot must have the role "bot". If it doesn't you will get the bug described in:
#https://github.com/RocketChat/Rocket.Chat/issues/16097
RemoteNickFormat="[{PROTOCOL}] <{NICK}> "

[[gateway]]
name="gateway1"
enable=true

[[gateway.inout]]
account="rocketchat.myrocketchat"
channel="prtocolchannel"

[[gateway.inout]]
account="rocketchat.myrocketchat"
channel="protocol2channel"

[[gateway.inout]]
account="api.myapi"
channel="api"
EOF
fi

echo "=> Ensure permissions"
chown -R cloudron:cloudron /run /app/data

echo "=> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
/usr/sbin/apache2 -DFOREGROUND &

exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i matterbridge
