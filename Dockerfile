FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

EXPOSE 8000

# add supervisor configs
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor-matterbridge.conf /etc/supervisor/conf.d/

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN a2disconf other-vhosts-access-log
ADD apache.conf /etc/apache2/sites-enabled/matterbridge.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN a2enmod proxy proxy_http rewrite

ARG MATTERBRIDGE_VERSION=1.17.5
RUN wget -O /usr/local/bin/matterbridge https://github.com/42wim/matterbridge/releases/download/v$MATTERBRIDGE_VERSION/matterbridge-$MATTERBRIDGE_VERSION-linux-64bit && \
    chmod +x /usr/local/bin/matterbridge && \
    matterbridge --version

# basic info page when opening the domain in a browser
COPY index.html /app/code/
COPY start.sh /app/pkg/

WORKDIR /app/data

CMD [ "/app/pkg/start.sh" ]
